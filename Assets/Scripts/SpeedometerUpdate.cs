using UnityEngine.UI;
using UnityEngine;


public class SpeedometerUpdate : MonoBehaviour
{
    float MapRange(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
    {
        float OldRange = (OldMax - OldMin);
        float NewRange = (NewMax - NewMin);
        float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

        return NewValue;
    }

    public CarController_NIS CarData;
    public Image Needle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //float currrentRPM = MapRange(900, 10000, 0, -270, CarData.currentRPM);
        //Quaternion q = Needle.rectTransform.rotation;
        //q.y = currrentRPM;

        //Needle.rectTransform.rotation = q;

        //Debug.LogError(currrentRPM);
    }
}
