using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Needle_Update : MonoBehaviour
{
    [SerializeField] Image Needle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Needle.rectTransform.eulerAngles = new Vector3(0, 0, Needle.transform.rotation.z);
    }
}
