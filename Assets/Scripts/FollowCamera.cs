using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public GameObject followTarget;
    public float cameraHeight = 0f;
    public float cameraDistance = 0f;
    public float smoothTime = 0.3f;
    private Vector3 velo = Vector3.zero;

    private Vector3 desiredLocation;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //desiredLocation = followTarget.transform.TransformPoint(0, cameraHeight, cameraDistance);
        //new Vector3(0, -cameraHeight, cameraDistance);
        //transform.position = desiredLocation;
        //transform.LookAt(followTarget.transform.position);

        desiredLocation = followTarget.transform.TransformPoint(0, cameraHeight, cameraDistance);

        transform.position = Vector3.SmoothDamp(transform.position, desiredLocation, ref velo, smoothTime);
        transform.LookAt(followTarget.transform.position);
    }
}
