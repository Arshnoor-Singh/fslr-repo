using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car_Controller_01 : MonoBehaviour
{
    private float turnInput;
    private float speedInput;
    public float power;
    public float maxTurnAngle;

    public WheelCollider wc_FL;
    public WheelCollider wc_FR;
    public WheelCollider wc_RL;
    public WheelCollider wc_RR;

    public Transform wm_FL;
    public Transform wm_FR;
    public Transform wm_RL;
    public Transform wm_RR;

    // Start is called before the first frame update
    void Start()
    {
        turnInput = 0f;
        speedInput = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        speedInput = (Input.GetAxis("Vertical")) * power;
        turnInput = (Input.GetAxis("Horizontal")) * maxTurnAngle;

        //code for applying speed
        wc_RL.motorTorque = speedInput;
        wc_RR.motorTorque = speedInput;
      
        //code for applying turning
        wc_FL.steerAngle = turnInput;
        wc_FR.steerAngle = turnInput;

        //code for updating wheel meshes
        wm_FL.position = wc_FL.transform.position;
        wm_FR.position = wc_FR.transform.position;
        wm_RL.position = wc_RL.transform.position;
        wm_RR.position = wc_RR.transform.position;

        wm_FL.rotation = wc_FL.transform.rotation;
        wm_FR.rotation = wc_FL.transform.rotation;
        wm_RL.rotation = wc_FL.transform.rotation;
        wm_RR.rotation = wc_FL.transform.rotation;
    }
}
